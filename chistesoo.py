##!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""

import xml.dom.minidom


def main(path):
    objeto = Humor(path)
    for joke in objeto.jokes():
        print(f"Calificación: {joke['Calificación']}.")
        print(f" Respuesta: {joke['Respuesta']}")
        print(f" Pregunta: {joke['Pregunta']}\n")


class Humor:

    def __init__(self, path):
        self.path = path

    def jokes(self):
        categorias = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']
        document = xml.dom.minidom.parse(self.path)
        jokes = document.getElementsByTagName('chiste')
        lista = []
        resultado = []

        for joke in jokes:
            score = joke.getAttribute('calificacion')
            questions = joke.getElementsByTagName('pregunta')
            question = questions[0].firstChild.nodeValue.strip()
            answers = joke.getElementsByTagName('respuesta')
            answer = answers[0].firstChild.nodeValue.strip()
            lista.append((score, question, answer))

        for n in categorias:
            for items in lista:
                n1 = items[0]
                if n1 == n:
                    resultado.append({'Calificación': n,
                                      'Respuesta': items[2],
                                      'Pregunta': items[1]})

        return resultado


if __name__ == "__main__":
    main('chistes.xml')
