##!/usr/bin/python3
# -*- coding: utf-8 -*-

import smil
import sys
import json


class SMILJSON(smil.SMIL):
    def json(self):
        lista = []
        archivo = smil.SMIL(self.path)

        for element in archivo.elements():
            element_obj = ElementJSON(element.name(), element.attrs(), element.cadena())
            lista.append(element_obj.dict())

        json_string = json.dumps(lista, indent=1)
        return json_string


class ElementJSON(smil.Element):

    def dict(self):
        json_dict = {'name': self.name(), 'attrs': self.diccionario }
        return json_dict


def main(path):
    obj = SMILJSON(path)
    obj.json()
    print(obj.json())


if __name__ == "__main__":
    main(sys.argv[1])

