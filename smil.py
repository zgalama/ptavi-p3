##!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom
import xml.etree.ElementTree


class Element:

    def __init__(self, elem, attrs, cadena):
        self.elemento = elem
        self.diccionario = attrs
        self.cadena = cadena

    def name(self):
        return self.elemento

    def attr(self):
        return self.diccionario


class SMIL:

    def __init__(self, path):
        self.path = path

    def elements(self):
        document = xml.etree.ElementTree.parse(self.path)
        elementos = []
        tipo = []

        for elemento in document.iter():
            elementos.append(elemento)

        for tipo_elem in elementos:
            tipo.append(Element(tipo_elem.tag, tipo_elem.attrib, elementos))

        return tipo


if __name__ == '__main__':
    objeto = SMIL('karaoke.smil')
    objeto_2 = objeto.elements()

