##!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""

import xml.dom.minidom

categorias = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']


def main(path):
    """Programa principal"""
    document = xml.dom.minidom.parse(path)
    if document.getElementsByTagName('humor'):
        jokes = document.getElementsByTagName('chiste')
        lista = []
   
        for joke in jokes:
            score = joke.getAttribute('calificacion')
            questions = joke.getElementsByTagName('pregunta')
            question = questions[0].firstChild.nodeValue.strip()
            answers = joke.getElementsByTagName('respuesta')
            answer = answers[0].firstChild.nodeValue.strip()
            lista.append((score, question, answer))

        for n in categorias:
            for items in lista:
                n1 = items[0]
                if n1 == n:
                    print(f"Calificación: {n}.")
                    print(f" Respuesta: {items[2]}")
                    print(f" Pregunta: {items[1]}\n")
                else:
                    pass
    else:
        raise Exception("Root element is not humor")


if __name__ == "__main__":
    main('tests/chistes.xml')
