##!/usr/bin/python3
# -*- coding: utf-8 -*-


import smil
import sys


def main(file):
    objeto = smil.SMIL(file)
    for elementos in objeto.elements():
        print(elementos.name())


if __name__ == '__main__':
    main(sys.argv[1])

